#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)

# symlinks scheme
IGNORE_PATTERN_HOME="(\.git)"

echo "Delete symlinks of home files..."

for f in $SCRIPT_DIR/home/.??*; do
  filename=$(basename $f)

  rm -v $HOME/$filename
done

echo "Done."
echo ""

echo "Create symlinks of neovim files..."

rm -v $HOME/.config/nvim

echo "Done."
echo ""
