#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)

# symlinks scheme
IGNORE_PATTERN_HOME="(\.git)"

echo "Create symlinks of home files..."

for f in $SCRIPT_DIR/home/.??*; do
  ln -sv $f $HOME
done

echo "Done."
echo ""

echo "Create symlinks of neovim files..."

ln -sv $SCRIPT_DIR/nvim/ $HOME/.config

echo "Done."
echo ""
