return {
  {
    "gbprod/nord.nvim",

    config = function()
      require("nord").setup({})
      vim.cmd.colorscheme("nord")
    end,
  },

  {
    "nvim-lualine/lualine.nvim",

    dependencies = "nvim-tree/nvim-web-devicons",

    opts = {
      options = {
        theme = "nord",
      },
    },
  },

  {
    "nvim-telescope/telescope.nvim",

    dependencies = "nvim-lua/plenary.nvim",

    config = function()
      local builtin = require("telescope.builtin")

      vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
      vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
      vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
      vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})

      require("telescope").setup({})
    end,
  },

  {
    "nvim-telescope/telescope-file-browser.nvim",

    dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },

    config = function()
      require("telescope").setup({
        extensions = {
          file_browser = {
            grouped = true,
            hidden = { file_browser = true, folder_browser = true },
            theme = "ivy",
            hijack_netrw = true,
            use_fd = true,
          },
        },

        pickers = {
          find_files = {
            hidden = true,
          },
        },
      })

      require("telescope").load_extension("file_browser")

      vim.keymap.set("n", "<space>fb", function()
        require("telescope").extensions.file_browser.file_browser()
      end)
    end,
  },

  {
    "lukas-reineke/indent-blankline.nvim",

    main = "ibl",

    opts = {},
  },

  {
    "nvim-treesitter/nvim-treesitter",

    build = ":TSUpdate",

    config = function()
      require("nvim-treesitter.configs").setup({
        ensure_installed = { "cpp", "python" },
        sync_install = true,

        highlight = {
          enable = true,
        },

        indent = {
          enable = false,
        },
      })
    end,
  },

  {
    "williamboman/mason-lspconfig.nvim",

    dependencies = {
      "williamboman/mason.nvim",

      "neovim/nvim-lspconfig",
    },

    config = function()
      require("mason").setup({})

      require("mason-lspconfig").setup({
        ensure_installed = { "lua_ls", "pylsp", "clangd", "rust_analyzer" },
        automatic_installation = true,
      })

      require("mason-lspconfig").setup_handlers({
        function(server_name)
          require("lspconfig")[server_name].setup({})
        end,
      })
    end,
  },

  {
    "jay-babu/mason-null-ls.nvim",

    dependencies = {
      "williamboman/mason.nvim",

      "nvimtools/none-ls.nvim",
      "nvimtools/none-ls-extras.nvim",
    },

    config = function()
      require("mason").setup({})

      require("mason-null-ls").setup({
        ensure_installed = { "stylua", "prettierd", "black", "clang-format", "pylint" },
        automatic_installation = true,
        handlers = {},
      })

      local null_ls = require("null-ls")

      null_ls.setup({
        sources = {
          -- linter
          null_ls.builtins.diagnostics.cppcheck,
        },

        on_attach = function(client, bufnr)
          if client.supports_method("textDocument/formatting") then
            vim.keymap.set("n", "<space>f", function()
              vim.lsp.buf.format({ async = false })
            end, bufopts)
          end
        end,
      })
    end,
  },

  {
    "hrsh7th/nvim-cmp",

    dependencies = {
      "hrsh7th/vim-vsnip",

      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-cmdline",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-vsnip",
    },

    config = function()
      local cmp = require("cmp")

      cmp.setup({
        snippet = {
          expand = function(args)
            vim.snippet.expand(args.body)
          end,
        },

        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },

        mapping = cmp.mapping.preset.insert({
          ["<C-Space>"] = cmp.mapping.complete(),
          ["<C-e>"] = cmp.mapping.abort(),
          ["<CR>"] = cmp.mapping.confirm({ select = false }),
        }),

        sources = cmp.config.sources({
          { name = "buffer",   max_item_count = 8 },
          { name = "nvim_lsp", max_item_count = 8 },
          { name = "cmdline",  max_item_count = 8 },
          { name = "vsnip",    max_item_count = 8 },
        }),
      })

      cmp.setup.cmdline("/", {
        mapping = cmp.mapping.preset.cmdline(),
        sources = {
          { name = "buffer", max_item_count = 8 },
        },
      })

      cmp.setup.cmdline(":", {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({
          { name = "path", max_item_count = 8 },
          {
            name = "cmdline",
            max_item_count = 8,
            option = {
              ignore_cmds = { "Man", "!" },
            },
          },
        }),
      })

      vim.g.vsnip_snippet_dir = vim.fn.stdpath("config") .. "/snippets/"
    end,
  },

  {
    "lewis6991/gitsigns.nvim",

    opts = {},
  },
}
