# My dotfiles

## Requirements

- Neovim >= 0.10.1
  - cppcheck
- fd
- Python >= 3.12
  - venv recommended
- nodeenv >= v22.5.1
  - for prettierd
- Deno >= 1.46.3
  - for bufpreview.vim
